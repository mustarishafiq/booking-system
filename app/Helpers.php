<?php

use App\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;



function login_alternative($request){

    if (isset($request->id)) {

        Cookie::forget('laravel_session');

        $faker = Faker::create();
        $staff_id = hash_url_decode($request->id);
        $staff_name = hash_url_decode($request->name);
        $staff_role = hash_url_decode($request->role);

        $user = User::where('staff_id',$staff_id)->first();

        if (!$user) {
            $users = [
                    'name'           => $staff_name,
                    'staff_id'       => $staff_id,
                    'username'       => $staff_id,
                    'email'          => $faker->companyEmail(),
                    'password'       => '$2y$10$VOunUZVMv4bwC84FJngVdORh/3WOQsLYXcHOTcmsVFWTVWWg.V8/i',
                    'remember_token' => null
            ];

            Cookie::queue(Cookie::forget('laravel_session'));
            $user_id = User::insertGetId($users);
            User::findOrFail($user_id)->roles()->sync($staff_role);
            Auth::loginUsingId($user_id, $remember = true);
        }else{
            Cookie::queue(Cookie::forget('laravel_session'));
            Auth::login($user);
            Auth::loginUsingId($user->id, $remember = true);
        }
    }
}

function hash_url_decode($decoded_id){
    $x = base64_decode(urldecode($decoded_id));
    $arr_x = explode("===", $x);
    $data_return = isset($arr_x[1]) ? $arr_x[1] : '';
    return $data_return;
}