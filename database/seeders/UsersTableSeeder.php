<?php

namespace Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Booking Admin',
                'staff_id'       => 'EMZ1',
                'username'       => 'admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$VOunUZVMv4bwC84FJngVdORh/3WOQsLYXcHOTcmsVFWTVWWg.V8/i',
                'remember_token' => null,
            ],
            [
                'id'             => 2,
                'name'           => 'Booking User',
                'staff_id'       => 'EMZ2',
                'username'       => 'user',
                'email'          => 'user@user.com',
                'password'       => '$2y$10$VOunUZVMv4bwC84FJngVdORh/3WOQsLYXcHOTcmsVFWTVWWg.V8/i',
                'remember_token' => null,
            ],
        ];

        User::insert($users);

    }
}
